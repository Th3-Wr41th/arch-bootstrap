#!/usr/bin/env sh

if [ "${TERM_PROGRAM}" = "tmux" ]; then
  echo "hyprpm has issues running inside of tmux, please run outside of tmux" >&2

  exit 1
fi

# Ensure headers are up to date
hyprpm update

# Add plugins

hyprpm add https://github.com/Duckonaut/split-monitor-workspaces

# --

# Enable plugins

hyprpm enable split-monitor-workspaces

# --

# Reload
hyprpm reload -n
