#!/bin/sh

# Sync repos
sudo pacman -Sy

# Install rust

sudo pacman --noconfirm -S rustup

rustup default stable-gnu

# --

CWD=$(pwd)

# Install paru

git clone https://aur.archlinux.org/paru.git "${HOME}/paru"

cd "${HOME}/paru" || { echo "failed to cd to paru dir" >&2; exit 1; }

makepkg -si

cd "${CWD}" || { echo "failed to cd to cwd" >&2; exit 1; }

rm -rf "${HOME}/paru"

# --

# Ensure system is to date
paru -Syu --noconfirm --skipreview

# Install packages
paru --noconfirm --skipreview --needed -S - < pkglist

# Enable sddm
sudo systemctl enable sddm

# Enable graphical target
sudo systemctl set-default graphical.target

# Grab my dotfiles
git clone --recursive https://gitlab.com/Th3-Wr41th/dotfiles "${HOME}/.dotfiles"

cd "${HOME}/.dotfiles" || { echo "failed to cd to dotfile dir" >&2; exit 1; }

./install.sh all

cd "${CWD}" || { echo "failed to cd to cwd" >&2; exit 1; }

# Setup gsettings
gsettings set org.gnome.desktop.interface gtk-theme 'Adwaita-dark'
gsettings set org.gnome.desktop.interface icon-theme 'Flat-Remix-Red-Dark'

# Setup sddm theme
sudo install -D -m 644 ~/.local/share/backgrounds/default.png /usr/share/backgrounds/default.png

sudo install -D -m 644 -o root -g root sddm/10-theme.conf /etc/sddm.conf.d/10-theme.conf
sudo install -D -m 644 -o root -g root sddm/theme.conf /usr/share/sddm/themes/sugar-dark/theme.conf

# Setup plymouth
sudo install -D -m 644 -o root -g root plymouth/plymouthd.conf /etc/plymouth/plymouthd.conf
sudo install -D -m 755 -o root -g root plymouth/bgrt-minimal /usr/share/plymouth/themes/bgrt-minimal

sudo cp /etc/default/grub /etc/default/grub.bak

sudo sh -c "echo 'GRUB_CMDLINE_LINUX_DEFAULT=\"\${GRUB_CMDLINE_LINUX_DEFAULT} splash\"' >> /etc/default/grub"

sudo sed --in-place=".bak" -e 's/base udev/base udev plymouth/g' /etc/mkinitcpio.conf

sudo plymouth-set-default-theme -R bgrt-minimal 

sudo grub-mkconfig -o /boot/grub/grub.cfg
