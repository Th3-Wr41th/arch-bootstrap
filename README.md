# Arch Bootstrap

Bootstap arch installation with my go to packages and my dotfiles.

## Install

To use run `archinstall` with the `--config` flag pointing to `https://gitlab.com/Th3-Wr41th/arch-bootstrap/-/raw/main/user_configuration.json`

On reboot:

```
$ git clone https://gitlab.com/Th3-Wr41th/arch-bootstrap.git

$ cd arch-bootstrap

$ ./install.sh
```

On login to hyprland:

```
$ cd arch-bootstrap

$ ./hyprland_first_run.sh
```
